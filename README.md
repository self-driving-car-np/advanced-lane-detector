# Advanced Lane Detector

This is the second project at this Nanodegree Program (NP).  It consists of highlighting the road lanes lines at the images fitting a polynomial and calculate its radius of curvature and car position related to center.

The code is in a single [Jupyter Notebook](./main.ipynb) and also vailable in an [HTML document](./main.html).

<a href="./output_videos/project_video.mp4" title="Link Title"><img src="./print.png" alt="Project Video" /></a>

</br>

The video above is an example of the generated output by the pipeline to the project video. Note that the test images are in [test_images](./test_images) folder and the test videos at root directory. Thus, the results are in the [test_videos_output](./test_videos_output) and [test_images_output](./test_images_output).

---
### 1. Presettings
Before going through the pipeline some parameters definition and preprocessing is needed. First the camera should be calibrated, then I should define the transformation to perform the birds-eye view. Also, in this stage I tweak the parameters to do the lane lines segmentation.

#### Camera calibration
Further all images entering the pipeline will be undistorted. To undistort an image we need the camera matrix obtained after the camera calibration.

The camera calibration is done below using the chessboard images in the same way as presented in Udacity's course.

The code is in the "Camera calibration" section of the [main.ipynb](./main.ipynb) notebook.

To verify the camera calibration let's undistort a chessboard image to see the original and the undistorted image:

<img src="./calibration.png">  

#### Birds-eye view transformation
In the pipeline we will transform every image to birds-eye view. To do it the transformation should be previosly defined. I will define it by taking an image of straight line road, identify the lane lines with the [simple lane detector](https://gitlab.com/self-driving-car-np/simple-lane-detector) and manually set the destination points to be centered.

So, the *undistorted image* with the lanes lines highlighted is:

<img src="./input_tranform.png">

Identifying the source points as `src` in the piece of the transform code bellow:
```python
src = [[215, 719], [568, 468], [715, 468], [1106, 719]]
dst = [[427., 720.], [427., 0.], [853., 0.], [853., 720.]]

transform1 = cv2.getPerspectiveTransform(np.array(src, dtype=np.float32), np.array(dst, dtype=np.float32))
transform2 = cv2.getPerspectiveTransform(np.array(src2, dtype=np.float32), np.array(dst, dtype=np.float32))

warped = cv2.warpPerspective(result, transform1, (result.shape[1], result.shape[0]), flags=cv2.INTER_LINEAR)
warped2 = cv2.warpPerspective(result, transform2, (result.shape[1], result.shape[0]), flags=cv2.INTER_LINEAR)
```

I have two transformations but in fact I will use just the transform 1, because the second one is two noisy. The image bellow show the result of birds-eye view, with the first transform at left:

<img src="./output_transform.png">

This is implementated in the "Transform image to birds-eye view" section of the [main.ipynb](./main.ipynb) notebook.

### 2. Pipeline description
Briefly, the pipeline consist of filtering operations to make a image with almost just the lanes on it. Then, the image is transformed to birds-eye view and the lane pixels are detected to calculate the curve radius and car position in the lane.

My pipeline consists of 5 steps:</br>
1. [Filtering operations](#filtering-operations)</br>
2. [Birds-eye view transform](#birds-eye-view-transform)</br>
3. [Fit the lane lines pixels](#fit-the-lane-lines-pixels)</br>
4. [Calculate radius and position](#calculate-radius-and-position)</br>
5. [Output final image](#output-final-image)</br>
</br>

The pipeline is defined in `pipeline` function which receives an image to detect and outputs an annotaded image highlighting the lane lines with curve radius and car position written on it.

---
#### Filtering operations
Filtering is the key of this pipeline, because the binary result of the thresholdings operation will be transformed to birds eye view and with that blank pixels the lane lines will be fitted.

The filtering is a result of gradient filters and color filters. They were extensily explored and are well explained at the [main.ipynb](./main.ipynb) notebook.

##### Gradient filtering

For gradient filtering many different thresholds parameters were tested for different "types of image". With types of image I mean the colorspace of the images. During Udacity's lessons we thresholded the gradients to grayscaled images, but since the HSL space is better to do color segmentation why not compute the gradients on it too?

Below the best result is shown, actually it is just using grayscaled images and the saturation channel:

<img src="./grad1.png">
<img src="./grad2.png">
<img src="./grad3.png">

The results of the gradient threshold show that to HSL images sobel X was more robust to filter the lane lines on diferent light conditions. In other hands, grayscaled thresholds were able to catch lanes far away where saturation failed. Thus, the best solution was to add both thresholds having a more robust and reliable result.

##### Color filtering

Filtering Edges with just gradient may be insuficient. To improve it let's see what kind of results we get thresholding in rgb and hsl colorspaces. The result below shows the best I got for color threshold.

<img src="./color.png">

Observing the results it is possible to see that RGB thresholds is able to get lines far away. But again we can not discard the saturation thresholds. So, by now I just add them.

##### Final filter

Now the objective is to see which filtering is performing better. Also, is desired to merge these filters, so it is necessary to analyse if is better to add them or make an intersection (bitwise and).

By now, after the tests, the code is this way:
```python
def gradient_filter(rgb, hls):
    # Separate the S channel
    s_channel = hls[:,:,2]
    
    # Apply the thresholding just at sobel x for saturation image channel
    s_sobels = sobels(s_channel, ksize)
    s_gradx = abs_sobel_thresh(s_sobels, orient='x', thresh=(15, 150))
    
    # Apply each of the thresholding functions to gray image
    gray = cv2.cvtColor(rgb, cv2.COLOR_RGB2GRAY)
    g_sobels = sobels(gray, ksize)
    gradx = abs_sobel_thresh(g_sobels, orient='x', thresh=(15, 150))
    grady = abs_sobel_thresh(g_sobels, orient='y', thresh=(15, 160))
    mag_binary = mag_thresh(g_sobels, mag_thresh=(15, 150))
    dir_binary = dir_threshold(g_sobels, thresh=(0.6, 1.3))
    
    # Merge the gray thresholds together    
    final = gradx * grady * mag_binary * dir_binary
    
    # Generate the result image
    combined_binary = np.zeros_like(final)
    combined_binary[(final == 1) | (s_gradx == 1)] = 1
    
    return combined_binary
    
def color_filter(rgb, hls):
    # Threshold RGB image
    white = cv2.inRange(rgb, (200, 200, 200), (255, 255, 255))
    yellow = cv2.inRange(rgb, (180, 180, 0), (255, 255, 150))
    color = white + yellow  
    
    # Threshold saturation channel
    lower = 160
    upper = 255
    s_binary = cv2.inRange(hls, (0, 0, lower), (180, 255, upper))
    
    # Generate the result image
    combined_binary = color + s_binary
    
    return combined_binary
```

The output of the 2 functions is:
<img src="./result_filter.png">

So, both filters have too much things than the lane lines, so we will keep the intersection between the images.

Also, the images are assumed to be already undistorted at the begining of the pipeline.

---
#### Birds-eye view transform

The transformation is defined in the presetting step. And after undistorting and doing the filters in the image the birds-eye transform is simple verified by:

```python
# Read in the image
image = mpimg.imread(folder + image_name)

# Undistort the image
image = cv2.undistort(image, mtx, dist, None, mtx)

# Apply the transform
warped = cv2.warpPerspective(image, transform1, (image.shape[1], image.shape[0]), flags=cv2.INTER_LINEAR)
```

---
#### Fit the lane lines pixels
After thresholding the image and having its birds-eye view the next step is to fit a polynomial to the line. The parameters found with the fit will be used to calculate line curvature.

There are many ways to fit a polynomial, we will use a simple least squares. Thus with the model fixed the key is the data (points) used, to get it I will use histograms and sliding windows just like the Udacity's lessons.

The code is "Fitting the polynomial" section of the [main.ipynb](./main.ipynb) notebook.

Bellow the output image of the fitting operation:

<img src="./fit_lane.png">

---
#### Calculate radius and position

With the parameters of the fitting the radius and position related to the center can be calculated. In fact, this are mixed with the fitting code.

There is nothing new here, just using the same steps as the Udacity's lessons.

---
#### Output final image

With everything calculated, it is time to display the final image. This is done after calling the `fit_polynomial` function, which receives a binary birds-eye view image.

```python
out_img, lp, rp, radius, shift = fit_polynomial(warped, False)
    
base = np.zeros_like(out_img)
base = cv2.polylines(base, lp, False, color=(255,0,0), thickness=13) 
base = cv2.polylines(base, rp, False, color=(255,0,0), thickness=13)
base = cv2.fillConvexPoly(base,  np.concatenate((lp[0], rp[0][::-1]), axis=0), color=(0,255,0))
orig_draw = cv2.warpPerspective(base, inverset1, (base.shape[1], base.shape[0]), flags=cv2.INTER_LINEAR)    

string1 = "Radius of curvature = %.2f (m)" %(radius)
string2 = "Vehicle is %.2fm left of center" %(shift)
cv2.putText(orig_draw, string1, (20, 60), cv2.FONT_HERSHEY_SIMPLEX, 2, color=(255,255,255), thickness=2)
cv2.putText(orig_draw, string2, (20, 130), cv2.FONT_HERSHEY_SIMPLEX, 2, color=(255,255,255), thickness=2)

final_result = weighted_img(orig_draw, image, α=0.8, β=1., γ=0.)
```

Let's put the final image and result of fit_polynomial side by side:

<img src="./final.png">
    
## 3. Potential shortcomings

This code don't work well on challeges datasets due to fail in lane lines segmentation. They are not good enought to work in many situations. Also, the curve radius isn't performing catastrophic but isn't reliable.


## 4. Possible improvements

The possibles improvements may come in an attempt to prevent the potential shortcomings. So, the filtering should be improved to fit all cases. The curve fitting should explores other strategies as well.
